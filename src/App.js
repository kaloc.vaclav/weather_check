import React, {Component} from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import WeatherForecast from './components/weather/WeatherForecast';
import HomePage from './components/HomePage'
import Header from './components/Header';
import './Background.css'

export default class App extends Component{
  render(){
    return(
      <BrowserRouter>
        <Header />
        <div className="Background">
          <Switch>
            <Route path="/" component={HomePage} exact />
            <Route path="/weather" component={WeatherForecast}/> 
            <Route path="/calculator" /> {/* TODO */}
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}