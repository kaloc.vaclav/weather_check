import React, {Component} from 'react'
import WeatherTemplate from './WeatherTemplate'
// import cities from 'cities.json'
import './Weather.css'

const URL = "http://api.openweathermap.org/data/2.5/forecast?q="
const UNIT = "&units=metric"
const APIKEY = "&appid=da0dc6eadbbb6ba045e0c806f05225cb"

export default class WeatherForecast extends Component {
  state = {
    lists: [],
    city: {},
    isLoading: false,
    error: null,
    value: "",
  }

  callAPI = (event) => {
    this.setState({ isLoading: true })
    event.preventDefault()

    fetch(URL + this.state.value + UNIT + APIKEY)
    .then(response => {
      if (response.ok){
        return response.json()
      }
      else{
          throw new Error("Shit happened")
      }
    })
    .then(data => this.setState({ city: data.city, lists: data.list, isLoading: false, value: "", error: "" }))

    .catch(error => this.setState({ error, isLoading: false}))
  }

  handleChange = (event) => {
   this.setState({
     value: event.target.value
   }) 
  }

  componentDidMount(){
    document.getElementById("input").focus()
  }
  
  render(){
    const { city, lists, isLoading, error } = this.state
    const weatherList = lists.map( (list, i) =>{
      return <WeatherTemplate 
      key={i} 
      date={list.dt_txt} 
      temp={list.main.temp}
      icon={list.weather[0].main}
      />
    })
    
    if(error){
      return(
        <div>
          <form className="submit-form" onSubmit={this.callAPI}>
          <input id="input" type="text" placeholder="Enter a city" value={this.state.value} onChange={this.handleChange}/>
          <input id="submit-button" type="submit" value="Submit" />
          </form>
          <h3>Invalid city</h3>
        </div>
      )
    }

    else if (isLoading){
      return(
        <div>
          Loading...
        </div>
      )
    }
    else{
      return(
        <div>
          <h2>5 day weather forecast</h2>
          <form className="submit-form" onSubmit={this.callAPI}>
            <input id="input" type="text" placeholder="Enter a city" value={this.state.value} onChange={this.handleChange}/>
            <input id="submit-button" type="submit" value="Submit" />
          </form>
          <h3>{city.name}</h3>
          <div className="all-weather">
          {weatherList}          
          </div>
        </div>
      )
    }
  }
}


