import React, {Component} from 'react'
import cloud from './img/cloud.png'
import rain from './img/rain.png'
import sun from './img/sun.png'
import snow from './img/snow.png'
import "./img.css"
import "./WeatherForecast.js"

export default class WeatherTemplate extends Component{

    
    render(){
        const imgSelect = () => {
             switch (this.props.icon) {
                 case "Clouds": return <img src={cloud} alt="Icon" />
                 case "Rain": return <img src={rain} alt="Icon" />
                 case "Snow": return <img src={snow} alt="Icon" />
                 default: return <img src={sun} alt="Icon" />
             }
         }

        let date = new Date(this.props.date)
        date = `${date.toString().substring(0, 10)} ${date.toString().substring(16, 21)}`
        return(
                <div className="weather"> 
                    <ul className="weather-list">
                        <li>{imgSelect()}</li>
                        <li>{`${date}`}</li>
                        <li>{`${this.props.temp}C`}</li>
                    </ul>
                </div>
        )
    }
}