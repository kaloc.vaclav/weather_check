import React, {Component} from 'react'
import { Link } from 'react-router-dom'

const headerStyle= {
  height: "50px",
  fontSize: "calc(10px + 2vmin)",
  textAlign: "center",
  color: "white",
  background: "black"
}

export default class Header extends Component{
  render(){
    return(
      <Link to="/" style={{textDecoration: "none"}}>
        <header style={headerStyle}>
          Go to Homepage
        </header>
      </Link>
    )
  }
}