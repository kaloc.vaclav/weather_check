import React, { Component } from 'react'
import { Link } from 'react-router-dom'

const buttonStyle = {
  backgroundColor: "#3F51B5", 
  border: "none",
  color: "white",
  padding: "50px",
  width: "300px",
  heigth: "300px",
  textAlign: "center",
  textDecoration: "none",
  fontSize: "16px",
  margin: "10px 15px",
  cursor: "pointer",
  borderRadius: "10px",
}

export default class HomePage extends Component{
  render(){
    return(
      <Link to="/weather"><button style={buttonStyle}>Weather Forecast!</button></Link>
    )
  }
}
